package Models;

public class Calculator {
    //Declarar atributos
    private float numberOne;
    private float numberTwo;

    //Definimos el constructor

    public Calculator(float numberOne, float numberTwo) {
        this.numberOne = numberOne;
        this.numberTwo = numberTwo;
    }

    //Definimos los metodos

    public float Addition(){
        return this.numberOne + this.numberTwo;
    }

    public float Subtraction(){
        return this.numberOne - numberTwo;
    }

    public float Multiply(){
        return this.numberOne * numberTwo;
    }

    public float Division(){
        return this.numberOne / numberTwo;
    }

}
