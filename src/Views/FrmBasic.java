package Views;

import Models.Calculator;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrmBasic extends JFrame {
    private JPanel pnlMain;
    private JTextField txtNumberOne;
    private JTextField txtNumberTwo;
    private JLabel lblNumberOne;
    private JLabel lblNumberTwo;
    private JTextField txtResult;
    private JButton btnAddition;
    private JButton btnSubtraction;
    private JButton btnMultiply;
    private JButton btnDivision;

    //Constructor
    public FrmBasic(){
        setContentPane(pnlMain);
        setSize(450,400);
        setTitle("Calculadora....");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);

        Calculator calculator;
        btnAddition.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                float num1 = Float.parseFloat(txtNumberOne.getText());
                float num2 = Float.parseFloat(txtNumberTwo.getText());
                Calculator calculator = new Calculator(num1, num2);
                txtResult.setText(String.valueOf(calculator.Addition()));
            }
        });

        btnSubtraction.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                float num1 = Float.parseFloat(txtNumberOne.getText());
                float num2 = Float.parseFloat(txtNumberTwo.getText());
                Calculator calculator = new Calculator(num1, num2);
                txtResult.setText(String.valueOf(calculator.Subtraction()));
            }
        });

        btnDivision.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                float num1 = Float.parseFloat(txtNumberOne.getText());
                float num2 = Float.parseFloat(txtNumberTwo.getText());
                Calculator calculator = new Calculator(num1, num2);
                txtResult.setText(String.valueOf(calculator.Division()));
            }
        });

        btnMultiply.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                float num1 = Float.parseFloat(txtNumberOne.getText());
                float num2 = Float.parseFloat(txtNumberTwo.getText());
                Calculator calculator = new Calculator(num1, num2);
                txtResult.setText(String.valueOf(calculator.Multiply()));
            }
        });

    }


}
